class Cell:
    def __init__(self):
        self.__alive = False
        self.__live_neighbors_count = 0

    def is_alive(self):
        return self.__alive

    def set_alive(self, be_alive):
        self.__alive = be_alive
