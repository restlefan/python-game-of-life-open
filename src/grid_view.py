from tkinter import Canvas, BOTH, Tk, Frame
from grid import Grid
from converter import Converter
from rx import Observer
import threading
import time
import sched


class GridView(Observer):
    def __init__(self, grid: Grid, cell_pixel_size: int = 10):
        self.__alive = True

        self.paint_scheduler = sched.scheduler(time.time, time.sleep)
        self.grid_scheduler = sched.scheduler(time.time, time.sleep)

        self.grid_time_between_runs = 0.25
        self.paint_time_between_runs = 1/60

        self.__blank_color = 'white'
        self.__filled_color = 'black'

        self.__frame = Tk()

        self.__grid = grid

        width_in_cells = len(grid.get_cells())
        height_in_cells = len(grid.get_cells()[0])

        self.__cell_pixel_size = cell_pixel_size
        self.__width_in_pixels = width_in_cells * cell_pixel_size
        self.__height_in_pixels = height_in_cells * cell_pixel_size

        self.__position_converter = Converter(self.__cell_pixel_size)

        self.__canvas = Canvas(
            self.__frame,
            height=self.__height_in_pixels,
            width=self.__width_in_pixels,
            bg='white')

        self.__frame.protocol('WM_DELETE_WINDOW', self.on_closing)
        self.__canvas.bind("<Button-1>", self.click_callback)
        self.__canvas.bind('<Configure>', self.__update_size)

        self.__canvas.pack(fill=BOTH, expand=True)
        self.__update_size(1)

    def start(self):

        self.__grid_thread = threading.Thread(
            target=self.queue_grid_tick_event)
        self.__view_thread = threading.Thread(target=self.queue_paint_event)

        self.__view_thread.start()
        self.__grid_thread.start()
        self.__canvas.mainloop()

    def queue_paint_event(self):
        if self.__alive:
            self.__paint()
            self.paint_scheduler.enter(
                self.paint_time_between_runs, 2, self.queue_paint_event)
            self.paint_scheduler.run()

    def queue_grid_tick_event(self):
        if self.__alive:
            self.__grid.tick()
            self.grid_scheduler.enter(self.grid_time_between_runs, 1,
                                      self.queue_grid_tick_event)
            self.grid_scheduler.run()

    def on_closing(self):
        self.__alive = False
        self.__grid.kill()
        self.__frame.quit()
        self.__frame.destroy()

    def __update_size(self, unused_parm):
        # self.__canvas.winfo_width()  # Get current width of canvas
        canvas_width = self.__width_in_pixels
        # self.__canvas.winfo_height()  # Get current height of canvas
        canvas_height = self.__height_in_pixels

        self.__canvas.delete('grid_line')  # Will only remove the grid_line
        # Creates all vertical lines at intevals of 100
        for i in range(0, canvas_width, self.__cell_pixel_size):
            self.__canvas.create_line(
                [(i, 0), (i, canvas_height)], tag='grid_line')

        # Creates all horizontal lines at intevals of 100
        for i in range(0, canvas_height,  self.__cell_pixel_size):
            self.__canvas.create_line(
                [(0, i), (canvas_width, i)], tag='grid_line')

    def __paint(self, event=None):
        # self.__canvas.winfo_width()  # Get current width of canvas
        canvas_width = self.__width_in_pixels
        # self.__canvas.winfo_height()  # Get current height of canvas
        canvas_height = self.__height_in_pixels

        for x in range(0, canvas_width, self.__cell_pixel_size):
            for y in range(0, canvas_height, self.__cell_pixel_size):
                cell_x, cell_y = self.__position_converter.pixel_position_to_cell(
                    x, y)
                if(self.__grid.cell_is_alive(cell_x-1, cell_y-1)):
                    self.draw_cell(x, y)
                else:
                    self.clear_cell(x, y)

    def coordinates_to_name(self, x, y):
        return 'cell'+str(x)+str(y)

    def draw_cell(self, x, y):
        self.__canvas.create_rectangle(
            x,
            y,
            x + self.__cell_pixel_size,
            y + self.__cell_pixel_size,
            fill=self.__filled_color,
            tag=self.coordinates_to_name(x, y))

    def clear_cell(self, x, y):
        # Will only remove the grid_line
        self.__canvas.delete(self.coordinates_to_name(x, y))

    def click_callback(self, event):
        conv_x, conv_y = self.__position_converter.pixel_position_to_cell(
            event.x, event.y)
        self.__grid.on_next(conv_x, conv_y)

    def tick(self):
        self.__paint()

    def on_next(self):
        self.__paint()

    def on_completed(self):
        print("Grid view Done!")

    def on_error(self, error):
        print("Grid view Error Occurred: {0}".format(error))
