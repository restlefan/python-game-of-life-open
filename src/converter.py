class Converter():
    def __init__(self, cell_pixel_size: int):
        self.__cell_pixel_size = cell_pixel_size

    def pixel_position_to_cell(self, pixel_x, pixel_y):
        return int(pixel_x / self.__cell_pixel_size), (pixel_y / self.__cell_pixel_size)

    def cell_position_to_pixel(self, idx_x, idx_y):
        return int(idx_x * self.__cell_pixel_size), int(idx_y * self.__cell_pixel_size)
