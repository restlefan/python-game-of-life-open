from grid import Grid
from grid_view import GridView
from shapes import glider, glider_gun
import time
import threading


class Runner:
    def __init__(self):
        self.__grid = Grid(60, 60)
        self.__grid.draw(1, 1, glider)

        self.start()

    def start(self):
        self.__grid_view = GridView(self.__grid)
        self.__grid_view.start()

    def complete(self):
        self.__grid.on_completed()
        self.__grid_view.on_completed()


r = Runner()
r.complete()
exit()
