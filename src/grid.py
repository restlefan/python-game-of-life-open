from cell import Cell
from rx import Observer
import time


class Grid(Observer):
    def __init__(self, x: int, y: int, wrap=False):
        self.__x = x
        self.__y = y
        self.initialize()

    def initialize(self):
        self.__tick_subscriber = None
        self.__is_alive = True
        self.__cells = [[0 for i in range(self.__x)] for j in range(self.__y)]
        self.__next_tick = [
            [0 for i in range(self.__x)] for j in range(self.__y)]

        for i in range(0, len(self.__cells)):
            for j in range(0, len(self.__cells[0])):
                self.__cells[i][j] = Cell()
                self.__next_tick[i][j] = False

    def is_alive(self):
        return self.__is_alive

    def kill(self):
        self.__is_alive = False

    def set(self, x: int, y: int):
        self.__cells[int(x)][int(y)].set_alive(True)

    def unset(self, x: int, y: int):
        self.__cells[int(x)][int(y)].set_alive(False)

    def draw(self, upper_left_x, upper_left_y, shape_arr):
        for i_x in range(0, len(shape_arr)):
            for i_y in range(0, len(shape_arr[0])):
                if shape_arr[i_x][i_y] == "x":
                    self.set(upper_left_x + i_x,
                             upper_left_y + i_y)

    def get_cells(self):
        return self.__cells

    def cell_is_alive(self, x: int, y: int):
        # print(x)
        # print(type(x))
        # print(y)
        # print(type(y))
        # print(self.__cells)
        # print(self.__cells[x])
        # print(self.__cells[x][y])
        return self.__cells[int(x)][int(y)].is_alive()

    def set_tick_subscriber(self, subscriber: Observer):
        self.__tick_subscriber = subscriber

    def get_tick_subscriber(self):
        return self.__tick_subscriber

    def tick(self):
        for i in range(len(self.__cells)):
            for j in range(len(self.__cells[i])):
                count = 0
                if i > 0:

                    if self.cell_is_alive(i - 1, j):
                        count += 1

                    if j < len(self.__cells[i]) - 1:
                        if self.cell_is_alive(i - 1, j + 1):
                            count += 1

                    if j > 0:
                        top_left = self.__cells[i - 1][j - 1]
                        if top_left.is_alive():
                            count += 1

                if j < len(self.__cells[i]) - 1:
                    bottom = self.__cells[i][j + 1]
                    if bottom.is_alive():
                        count += 1

                if i < len(self.__cells) - 1:
                    right = self.__cells[i + 1][j]
                    if right.is_alive():
                        count += 1
                    if j > 0:
                        top_right = self.__cells[i + 1][j - 1]
                        if top_right.is_alive():
                            count += 1

                    if j < len(self.__cells[i]) - 1:
                        bottom_right = self.__cells[i + 1][j + 1]
                        if bottom_right.is_alive():
                            count += 1

                if j > 0:
                    upper = self.__cells[i][j - 1]
                    if upper.is_alive():
                        count += 1

                if count < 2:
                    self.__next_tick[i][j] = False
                if self.__cells[i][j].is_alive() and (count == 2 or count == 3):
                    self.__next_tick[i][j] = True
                if self.__cells[i][j].is_alive() and count > 3:
                    self.__next_tick[i][j] = False
                if not self.__cells[i][j].is_alive() and count == 3:
                    self.__next_tick[i][j] = True

        for i in range(0, len(self.__cells)):
            for j in range(0, len(self.__cells[i])):
                self.__cells[i][j].set_alive(self.__next_tick[i][j])

        if not self.__tick_subscriber is None:
            self.__tick_subscriber.on_next()
        # self.__ascii_paint()

    def __ascii_paint(self):
        for i in range(0, len(self.__cells)):
            to_print = ""
            for j in range(0, len(self.__cells[i])):
                self.__cells[i][j].set_alive(self.__next_tick[i][j])
                if(self.__cells[i][j].is_alive()):
                    to_print += "O"
                else:
                    to_print += " "
            print(to_print)

    def on_next(self, x: int, y: int):
        self.set(x, y)

    def on_completed(self):
        print("Grid DoNe!")

    def on_error(self, error):
        print("Error Occurred: {0}".format(error))
